# PVOG Lebenslagen Icons

Repository für die im Suchdienst-Client (Referenzimplementierung PVOG-Suche) verwendeten Icons in der Lebenslagensuche. Die Dateinamen der Icons entsprechen dabei den Kurzbeschreibungen der Hauptlagen der Codeliste [PV-Lagen](https://www.xrepository.de/details/urn:xoev-de:fim:codeliste:pvlagen).
